#!/bin/sh
# BASH script for calling Ansible Tower APIs
#
# Ensure environment variable is set for the Ansible Tower token 
# e.g. 
# export TOKEN=<token>
#
# 
# These can be tweaked for thr number of attempts and the time it waits before checking again
DOMAIN=$ANSIBLE_TOWER_DOMAIN
MAX_TRYS=5
SLEEP_SECONDS=5

# The token could be hardcoded for testing, just never leave a real value in here commited to version control! 
#TOKEN=<token>

# Ansible Tower APIs are referencse by there number, this needs to be passed to this script!
JOB_TEMPLATE=$1

if [[ -z "$JOB_TEMPLATE" ]];then
  echo "You must provide a job template number, example ./api_job_template.sh 42"
  exit
fi

# Example Usage
# ./api_launch_job_template 14

# Main Script
TRYS=0
# echo "Using token: $TOKEN"
echo "Using job template: $TOWER_API_TERRAFORM_APPLY"

URL=`curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" -X POST  -d '{}' "https://$DOMAIN/api/v2/job_templates/$JOB_TEMPLATE/launch/" | jq -r .url`

echo "Launched Ansible Tower Job Template, URL is $URL"

if [[ -z "$URL" ]];then
  echo "Something went wrong with the API call...nothing returned???"
  exit
fi

STATUS=`curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" -X GET -d '{}' "https://$DOMAIN$URL" | jq -r .status`

echo "Current status is: $STATUS"

while [[ "$STATUS" != "successful" ]]
do
  echo "Waiting for success, current status is: $STATUS"
  echo "Number of trys: $TRYS"
  let "TRYS++"
  if [[ $TRYS == $MAX_TRYS ]]; then
    echo "Tried $TRYS times, quitting."
    exit
  fi
  sleep $SLEEP_SECONDS
  STATUS=`curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" -X GET -d '{}' "https://$DOMAIN$URL" | jq -r .status`
done

echo "Script Completed, ending status was: $STATUS"
