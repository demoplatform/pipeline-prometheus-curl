#!/bin/sh
# BASH script for calling Ansible Tower APIs
#
# Example Usage
# ./api_job_template_single_extra_var.sh 14 <extra_var_name> <extra_var_value>
#
# Ensure environment variable is set for the Ansible Tower token 
# e.g. 
# export TOKEN=<token>
#
# 
# These can be tweaked for thr number of attempts and the time it waits before checking again
DOMAIN=$ANSIBLE_TOWER_DOMAIN
MAX_TRYS=20
SLEEP_SECONDS=5

# The token could be hardcoded for testing, just never leave a real value in here commited to version control! 
#TOKEN=<token>

# Ansible Tower APIs are referencse by there number, this needs to be passed to this script!
JOB_TEMPLATE=$1

if [[ -z "$JOB_TEMPLATE" ]];then
  echo "You must provide a job template number, example ./api_job_template_single_extra_var.sh 42 myvar 123"
  exit
fi

# Specific params for adding Promethues Alert Rules
extra_var_name=$2  
extra_var_value=$3              
               
if [[ -z "$extra_var_name" ]];then
  echo "You must provide an extra var name!"
  exit
fi

if [[ -z "$extra_var_value" ]];then
  echo "You must provide an extra var value!"
  exit
fi

# Main Script
TRYS=0
# echo "Using token: $TOKEN"

# Call Ansible Tower API
URL=`curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" -X POST -d '{"extra_vars": "{\"'$extra_var_name'\": \"'$extra_var_value'\"}"}' "https://$DOMAIN/api/v2/job_templates/$JOB_TEMPLATE/launch/" | jq -r .url`

echo "Launched Ansible Tower Job Template, URL is $URL"

if [[ -z "$URL" ]];then
  echo "Something went wrong with the API call...nothing returned???"
  exit
fi

STATUS=`curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" -X GET -d '{}' "https://$DOMAIN$URL" | jq -r .status`

echo "Current status is: $STATUS"

while [[ "$STATUS" != "successful" ]]
do
  echo "Waiting for success, current status is: $STATUS"
  echo "Number of trys: $TRYS"
  let "TRYS++"
  if [[ $TRYS == $MAX_TRYS ]]; then
    echo "Tried $TRYS times, quitting."
    exit
  fi
  if [[ "$STATUS" == "failed"]]; then
    echo "The Job failed. Check Ansible Tower to investigate the fail job."
    exit
  fi
  if [[ "$STATUS" == "failed"]]; then
    echo "The Job failed. Check Ansible Tower to investigate the fail job."
    exit
  fi
  sleep $SLEEP_SECONDS
  STATUS=`curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" -X GET -d '{}' "https://$DOMAIN$URL" | jq -r .status`
done

echo "Script Completed, ending status was: $STATUS"
